package ricm.babystep.printer;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Printer extends UnicastRemoteObject implements IPrinter {

	public Printer() throws RemoteException {
		super();
	}
	
	@Override
	public String toString() {
		return "This is a printer";
	}
	
	@Override
	public void printLine(String s) throws RemoteException {
		System.out.println(s);
	}
	
}
