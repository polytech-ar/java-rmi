package ricm.babystep;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import ricm.babystep.printer.IPrinter;

public class Client {
	private IPrinter p;
	private Registry reg;
	
	public Client(String name, String txt) throws RemoteException, NotBoundException {
		reg = LocateRegistry.getRegistry(1099);
		p = (IPrinter) reg.lookup(name);
		System.out.println(p.toString());
		p.printLine(txt);
	}
	
	public static void main(String[] args) throws RemoteException, NotBoundException {
		new Client("//localhost/printer", "Hello World!");
	}
}
