package ricm.babystep;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import ricm.babystep.printer.Printer;

public class Server {
	
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		Printer  p;
		Registry reg;
		
		p   = new Printer();
		reg = LocateRegistry.createRegistry(1099);
		
		reg.rebind("//localhost/printer", p);
	}
}
