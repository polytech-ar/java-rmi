package chat;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import chat.chat.IChatRoom;
import chat.chat.IParticipant;
import chat.chat.Participant;

public class Client implements Runnable {
	
	private final static char   COMMAND_CHAR = ':';
	private final static String WHO_CMD      = "who";
	private final static String LEAVE_CMD    = "leave";
	private final static String WHICH_CMD    = "which";
	
	private IChatRoom    cr;
	private IParticipant p;
	private boolean      exit = false;
	
	
	public Client(String name, IChatRoom cr) throws RemoteException {
		p       = new Participant(name);
		this.cr = cr;
		this.cr.connect(p);
	}
	
	public void send(String msg) throws RemoteException {
		cr.send(p, msg);
	}
	
	public void leave() throws RemoteException {
		cr.send(p, "A quitté la chatroom");
		cr.leave(p);
		System.out.println("Good bye!");
		exit = true;
	}
	
	public void who() throws RemoteException {
		String participants[] = cr.who();
		System.out.println("\nParticipants:");
		
		for (String n : participants) {
			String who = "- " + n;
			
			if (n.equals(p.name()))
				who += " (moi)";
			
			System.out.println(who);
		}
		
		System.out.println("");
	}
	
	public void which() throws RemoteException {
		System.out.println("\n Chatroom: " + cr.name() + "\n");
	}
	
	public void run() {
		
		while (!exit) {
			System.out.print(">> ");
			String cmd = System.console().readLine();
			
			try {
				
				if (cmd.charAt(0) == COMMAND_CHAR) {
					cmd = cmd.substring(1, cmd.length());
					
					switch (cmd) {
						case WHO_CMD:
							this.who();
							break;
						case LEAVE_CMD:
							this.leave();
							break;
						case WHICH_CMD:
							this.which();
							break;
						default:
							throw new IllegalArgumentException(cmd + " Is not defined");
					}
					
				} else {
					this.send(cmd);
				}
				
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static void main(String[] args) throws RemoteException, NotBoundException {
		
		if (args.length < 2) {
			throw new IllegalArgumentException("The user name and the chatroom name are missing.");
		}
		
		String uname = args[0];
		String chatroom = args[1];
		
		String   name = "//localhost/"+chatroom;
		Registry reg  = LocateRegistry.getRegistry(1099);
		
		Client c = new Client(uname, (IChatRoom) reg.lookup(name));
		
		Thread th = new Thread(c);
		th.run();
		
	}
	
}
