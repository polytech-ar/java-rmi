package chat;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Map;

import chat.chat.ChatRoom;
import chat.chat.IChatRoom;

public class Server {
	
	private Map<String, IChatRoom> crs;
	private Registry reg;
	
	public Server() throws RemoteException {
		crs = new HashMap<String, IChatRoom>();
		reg = LocateRegistry.createRegistry(1099);
	}
	
	public void newChatRoom(String name) throws RemoteException {
		String key = "//localhost/"+name;
		crs.put(name, new ChatRoom(name));
		reg.rebind(key, crs.get(name));
	}
	
	public IChatRoom getChatRoom(String name) throws RemoteException {
		if(crs.containsKey(name)) {
			return crs.get(name);
		}else {
			newChatRoom(name);
			return crs.get(name);
		}
	}
	
	public static void main(String[] args) throws RemoteException {
		Server srv = new Server();
		
		while(true) {
			System.out.print("SERVE>> ");
			String chatroom = System.console().readLine();
			srv.newChatRoom(chatroom);
			System.out.println("SERVE>> Chatroom created");
		}
	}

}
