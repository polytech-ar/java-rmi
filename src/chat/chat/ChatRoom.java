package chat.chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ChatRoom extends UnicastRemoteObject implements IChatRoom {
	
	private String             name;
	private List<IParticipant> participants;
	
	
	public ChatRoom(String name) throws RemoteException {
		this.name         = name;
		this.participants = new ArrayList<IParticipant>();
	}
	
	@Override
	public String name() throws RemoteException {
		return name;
	}
	
	@Override
	public String[] who() throws RemoteException {
		String[] names = new String[participants.size()];
		
		for (int i = 0; i < participants.size(); i++) {
			names[i] = participants.get(i).name();
		}
		
		return names;
	}
	
	@Override
	public void connect(IParticipant p) throws RemoteException {
		participants.add(p);
	}
	
	@Override
	public void leave(IParticipant p) throws RemoteException {
		participants.remove(p);
	}
	
	@Override
	public void send(IParticipant p, String msg) throws RemoteException {
		Iterator<IParticipant> ite = participants.iterator();
		
		while (ite.hasNext()) {
			IParticipant pa = ite.next();
			if(!p.name().equals(pa.name()))
				pa.receive(p.name(), msg);
		}
		
	}
	
}
