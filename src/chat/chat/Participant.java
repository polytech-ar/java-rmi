package chat.chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Participant extends UnicastRemoteObject implements IParticipant {
	
	private String name;
	
	
	public Participant(String name) throws RemoteException {
		this.name = name;
	}
	
	@Override
	public String name() throws RemoteException {
		return name;
	}
	
	@Override
	public void receive(String name, String msg) throws RemoteException{
		System.out.println(name + ": " + msg);
		System.out.print(">> ");
	}
	
}
